import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component';
import PizzaForm from './pages/PizzaForm';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList(data),
	aboutPage = new Component('p', null, 'ce site est génial'),
	pizzaForm = new PizzaForm();

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];
Router.menuElement = document.querySelector('.mainMenu');

Router.navigate('/'); // affiche la liste des pizzas

document.querySelector('.logo').innerHTML +=
	"<small>les pizzas c'est la vie</small>";

let firstLink = document.querySelector('.mainMenu a');
firstLink.setAttribute('class', firstLink.getAttribute('class') + ' active');

let newsContainer = document.querySelector('.newsContainer');
displayNewsContainer();

let closeButton = document.querySelector('.closeButton');

if (closeButton) {
	closeButton.addEventListener('click', event => {
		event.preventDefault();
		closeNewsContainer();
	});
}

function displayNewsContainer() {
	newsContainer.style = '';
}

function closeNewsContainer() {
	newsContainer.style = 'display:none';
}

let links = document.querySelectorAll('.mainMenu a');

if (links) {
	for (let link of links) {
		link.addEventListener('click', event => {
			event.preventDefault();
			Router.navigate(link.getAttribute('href'));
		});
	}
}
