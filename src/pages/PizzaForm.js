import Page from './Page.js';

export default class PizzaForm extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);
		const form = document.querySelector('form'),
			input = form.querySelector('input[name=name]');

		form.addEventListener('submit', event => {
			event.preventDefault();
			console.log('name :', input.value);
			if (input.value) {
				alert('La pizza ' + input.value + ' a été ajoutée');
				input.value = '';
			} else {
				alert('Veuillez entrer un nom pour votre pizza !');
				input.focus();
			}
		});
	}

	submit(event) {}
}
